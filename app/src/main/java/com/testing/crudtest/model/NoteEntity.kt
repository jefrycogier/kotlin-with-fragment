package com.testing.crudtest.model

import androidx.room.*

@Entity(tableName = "datastaff")
data class NoteEntity(
    //PrimaryKey annotation to declare primary key
    //ColumnInfo annotation to specify the column's name
    @PrimaryKey(autoGenerate = true)@ColumnInfo(name = "id") var id: Int = 0,
    @ColumnInfo(name = "name") var name: String = "",
    @ColumnInfo(name = "positions") var positions: String = "",
    @ColumnInfo(name = "status") var status: String = "",
    @ColumnInfo(name = "address") var address: String = ""
)

@Dao
interface NoteDao {

    @Insert
    fun insert(note: NoteEntity)

    @Update
    fun update(note: NoteEntity)

    @Delete
    fun delete(note: NoteEntity)

    @Query("SELECT * FROM datastaff")
    fun getAll() : List<NoteEntity>

    @Query("SELECT * FROM datastaff WHERE id = :id")
    fun getById(id: Int) : List<NoteEntity>

    @Query("DELETE FROM datastaff WHERE id = :id")
    fun deleteById(id: Int)

    @Query("SELECT * FROM datastaff WHERE name = :name")
    fun getByName(name: String) : List<NoteEntity>
}