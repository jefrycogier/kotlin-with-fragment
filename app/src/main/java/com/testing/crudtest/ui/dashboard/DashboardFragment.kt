package com.testing.crudtest.ui.dashboard

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.Toast
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import com.testing.crudtest.R
import com.testing.crudtest.helper.SessionManager
import com.testing.crudtest.ui.MainActivity
import kotlinx.android.synthetic.main.act_lstener.*
import java.util.*

class DashboardFragment: Fragment(){
    companion object {
        fun instance(): DashboardFragment {
            return DashboardFragment()
        }
    }

    private val _btnLihatDataApi: Button? by lazy {
        view?.findViewById(R.id.btnLihatDataApi)
    }
    private val _btnAddDataDummy: Button? by lazy {
        view?.findViewById(R.id.btnAddDataDummy)
    }
    private val _btnLihatDataDummy: Button? by lazy {
        view?.findViewById(R.id.btnLihatDataDummy)
    }
    private val _btnLogout: Button? by lazy {
        view?.findViewById(R.id.btnLogout)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.activity_dashboard, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        // check if session is active
        val currentTime = Calendar.getInstance().time
        context?.let {
            val sessionStatus = SessionManager.isSessionActive(currentTime, it)
            val token = SessionManager.getUserToken(it)
            //textToken.text = token
            //textSessionStatus.text = sessionStatus.toString()
        }

        _btnLihatDataApi?.setOnClickListener{
            Toast.makeText(context, "Lihat API", Toast.LENGTH_SHORT).show()
        }
        _btnAddDataDummy?.setOnClickListener {
            (activity as MainActivity).onViewdata()
        }
        _btnLihatDataDummy?.setOnClickListener{
            (activity as MainActivity).onInputdata()
        }
        _btnLogout?.setOnClickListener {
            context?.let { SessionManager.endUserSession(it) }
            (activity as MainActivity).onLogout()
        }
    }
}