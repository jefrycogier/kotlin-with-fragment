package com.testing.crudtest.data

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.testing.crudtest.R
import com.testing.crudtest.config.MyDatabase
import com.testing.crudtest.model.NoteDao
import com.testing.crudtest.model.NoteEntity
import com.testing.crudtest.ui.MainActivity
import kotlinx.android.synthetic.main.activity_input.*


class InputFragment : Fragment() {

    private var isUpdate = false
    //private lateinit var database: NoteRoomDatabase
    private lateinit var dat: NoteEntity
    private lateinit var data: NoteDao
    private var idstaff = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        getData()

        val root = inflater.inflate(R.layout.activity_input, container, false)


        val btn = root.findViewById<View>(R.id.button_save)
        val btnDelete = root.findViewById<View>(R.id.button_delete)
        btn.setOnClickListener {
            val name = edit_text_name.text.toString()
            val positions = edit_text_positions.text.toString()
            val status = edit_text_status.text.toString()
            val address = edit_text_address.text.toString()

            if (name.isEmpty() && positions.isEmpty() && status.isEmpty() && address.isEmpty()){
                Toast.makeText(context!!, "Data cannot be empty", Toast.LENGTH_SHORT).show()
            }
            else{
                if (isUpdate){
                    data.update(NoteEntity(id = id, name = name, positions = positions, status = status, address = address))
                    Log.i("Updated data ", "name: " + name)
                    Toast.makeText(requireContext(), "Data " + name + " Updated", Toast.LENGTH_SHORT).show()
                    (activity as MainActivity).onViewdata()
                }
                else{
                    data.insert(NoteEntity(name = name, positions = positions, status = status, address = address))
                    Log.i("saved data ", "name: " + name)
                    Toast.makeText(requireContext(), "Data " + name + " saved", Toast.LENGTH_SHORT).show()
                    (activity as MainActivity).onViewdata()
                }
            }
        }

        btnDelete.setOnClickListener {
            data.deleteById(id)
            Toast.makeText(context!!, "Data removed", Toast.LENGTH_SHORT).show()
            (activity as MainActivity).onViewdata()
        }


        return root

        //return inflater.inflate(R.layout.fragment_input, container, false)
    }

    private fun getData(){

        //database = NoteRoomDatabase.getDatabase(getActivity())
        data = MyDatabase.getDatabase(context!!).daoNote()

        try{
            val bundle = this.arguments
            if (bundle != null) {
                button_delete.visibility = View.VISIBLE
                isUpdate = true

                idstaff = bundle.getInt("MainActId", 0) //0 is the default value , you can change that
                val name = bundle.getString("MainActName", "")
                val positions = bundle.getString("MainActPositions", "")
                val status = bundle.getString("MainActStatus", "")
                val address = bundle.getString("MainActAddress", "")

                edit_text_name.setText(name)
                edit_text_positions.setText(positions)
                edit_text_status.setText(status)
                edit_text_address.setText(address)
            }

            Log.i("load(form edit act)", "id: " + id)
        }catch (ex: Exception){
            Log.i("error exception", "catch " + ex)
        }


    }
}