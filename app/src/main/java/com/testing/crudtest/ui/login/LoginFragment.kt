package com.testing.crudtest.ui.login

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.testing.crudtest.R
import com.testing.crudtest.helper.SessionManager
import com.testing.crudtest.ui.dashboard.DashboardFragment
import com.testing.crudtest.ui.MainActivity


class LoginFragment: Fragment() {

    companion object {
        fun instance(): LoginFragment {
            return LoginFragment()
        }
    }

    private val _emailText: EditText? by lazy {
        view?.findViewById(R.id.usrusr)
    }
    private val _passwordText: EditText? by lazy {
        view?.findViewById(R.id.pswrdd)
    }
    private val _loginButton: TextView? by lazy {
        view?.findViewById(R.id.lin)
    }
    private val _progressbar: ProgressBar? by lazy {
        view?.findViewById(R.id.progress_bar)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.activity_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        _loginButton?.setOnClickListener {
            login()
            (activity as MainActivity).onDashboard()
        }
    }

    fun login() {
        Log.d(javaClass.simpleName, "Login")

        if (!validate()) {
            onLoginFailed()
            return
        }

        _loginButton?.isEnabled = false

        Handler(Looper.getMainLooper()).postDelayed(
            {
                // On complete call either onLoginSuccess or onLoginFailed
                onLoginSuccess()
                // onLoginFailed();
                //progressDialog.dismiss()
            }, 3000)
    }

    fun onLoginSuccess() {
        _loginButton?.isEnabled = true
        // Variable to hold progress status
        var progressStatus = 0;

        // Initialize a new Handler instance
        val handler: Handler = Handler()

        // Start the lengthy operation in a background thread
        Thread(Runnable {
            while (progressStatus < 100) {
                // Update the progress status
                progressStatus += 90

                // Try to sleep the thread for 50 milliseconds
                try {
                    Thread.sleep(50)
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                }

                // Update the progress bar
                handler.post(Runnable {
                    _progressbar?.progress = progressStatus
                })
            }
        }).start() // Start the operation

        //set session
        context?.let {
            SessionManager.startUserSession(it, 10000)
            SessionManager.storeUserToken(it, "randomUserToken")
        }
    }

    fun onLoginFailed() {
        Toast.makeText(context, "Login failed", Toast.LENGTH_LONG).show()

        _loginButton?.isEnabled = true
    }

    private fun validate(): Boolean {
        var valid = true

        val email = _emailText?.text?.toString()
        val password = _passwordText?.text?.toString()

        if (email.isNullOrBlank() || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            _emailText?.error = "enter a valid email address"
            valid = false
        } else {
            _emailText?.error = null
        }

        if (password.isNullOrBlank() || password.length < 4 || password.length > 10) {
            _passwordText?.error = "between 4 and 10 alphanumeric characters"
            valid = false
        } else {
            _passwordText?.error = null
        }

        return valid
    }
}