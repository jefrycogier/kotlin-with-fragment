package com.testing.crudtest.ui.data

import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.testing.crudtest.R
import com.testing.crudtest.adapter.DataAdapter
import com.testing.crudtest.config.MyDatabase
import com.testing.crudtest.helper.SessionManager
import com.testing.crudtest.model.NoteDao
import com.testing.crudtest.model.NoteEntity
import com.testing.crudtest.ui.MainActivity
import org.jetbrains.anko.startActivity
import java.util.*


class DatalistFragment: Fragment(){
    /*companion object {
        fun instance(): DatalistFragment {
            return DatalistFragment()
        }
    }*/
    private lateinit var data: NoteDao
    private lateinit var listarray: ArrayList<NoteDao>


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val root = inflater.inflate(R.layout.activity_list, container, false)


        val btnCreate = root.findViewById<View>(R.id.floatingActionButton)

        btnCreate.setOnClickListener{
            (activity as MainActivity).onInputdata()
        }

        val database = MyDatabase.getDatabase(context!!)
        val dao = database.daoNote()
        val listItems = arrayListOf<NoteEntity>()
        listItems.addAll(dao.getAll())
        Log.i("load(form datalist act)", "data: " + listItems)


        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        // check if session is active
        val currentTime = Calendar.getInstance().time
        context?.let {
            val sessionStatus = SessionManager.isSessionActive(currentTime, it)
            val token = SessionManager.getUserToken(it)
        }


    }

}