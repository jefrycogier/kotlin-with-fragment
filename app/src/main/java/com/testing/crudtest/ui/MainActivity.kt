package com.testing.crudtest.ui

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.testing.crudtest.R
import com.testing.crudtest.config.MyDatabase
import com.testing.crudtest.data.InputFragment
import com.testing.crudtest.model.NoteEntity
import com.testing.crudtest.ui.dashboard.DashboardFragment
import com.testing.crudtest.ui.data.DatalistFragment
import com.testing.crudtest.ui.login.LoginFragment
import kotlinx.android.synthetic.main.act_lstener.*
import kotlinx.android.synthetic.main.activity_list.*

class MainActivity: AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.fragment, LoginFragment.instance())
        fragmentTransaction.commit()
    }

    internal fun onDashboard() {
        supportFragmentManager.beginTransaction()
            .replace(R.id.fragment, DashboardFragment())
            .commitNow()
    }

    internal fun onLogout() {
        supportFragmentManager.beginTransaction()
            .replace(R.id.fragment, LoginFragment())
            .commitNow()
    }

    internal fun onInputdata() {
        supportFragmentManager.beginTransaction()
            .replace(R.id.fragment, InputFragment())
            .commitNow()
    }

    internal fun onInputdatafromlist() {
        supportFragmentManager.beginTransaction()
            .replace(R.id.fragment, InputFragment())
            .commitNow()
    }

    internal fun onViewdata() {
        supportFragmentManager.beginTransaction()
            .replace(R.id.fragment, DatalistFragment())
            .commitNow()
    }


    internal fun getDataArray() {
        // data yang akan kita tampilkan ke dalam ListView
        val languages = listOf("Java", "Kotlin", "Javascript", "PHP", "Python")
        Log.i("load(data dari array)", "data array: " + languages)

        // memberikan adapter ke ListView
        //lv_languages.adapter = ArrayAdapter(applicationContext,android.R.layout.simple_list_item_1,languages)


    }

        internal fun getNotesData() {
            val database = MyDatabase.getDatabase(applicationContext)
            val dao = database.daoNote()
            val listItems = arrayListOf<NoteEntity>()
            listItems.addAll(dao.getAll())

            if (listItems.isNotEmpty()) {
                text_view_note_empty.visibility = View.GONE
            } else {
                text_view_note_empty.visibility = View.VISIBLE
            }
        }

    override fun onBackPressed() {
        val navHost = supportFragmentManager.findFragmentById(R.id.btnLogout)
        navHost?.let { navFragment ->
            navFragment.childFragmentManager.primaryNavigationFragment?.let { fragment ->
                if (fragment is DashboardFragment) {
                    //finish()
                    Log.i("is true", "ini if")
                } else {
                    Log.i("is false", "ini else")
                    //super.onBackPressed()
                }
            }
        }
    }


}

